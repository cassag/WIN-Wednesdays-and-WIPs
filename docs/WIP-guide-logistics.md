---
layout: default
title: Logistics
parent: WIP guide
has_children: false
nav_order: 2
---

# Logistics
{: .fs-9 }

The when, where and how of the WIP
{: .fs-6 .fw-300 }

---

**Contents**
- [When should I present a WIP?](#when-should-i-present-a-wip)
- [Where does the WIP take place?](#where-does-the-wip-take-place)
- [How do I sign up for a WIP presentation?](#how-do-i-sign-up-for-a-wip-presentation)
- [How should I promote my WIP?](#how-should-i-promote-my-wip)

## When should I present a WIP?

It is best to give a WIP presentation as **early as possible** in your project. This will enable you to receive and incorporate valuable feedback effectively.

WIPs must be completed minimally 2 weeks before planned data collection for new MRI and MEG projects. You can present ideas for planned projects before data collection, or ethics approvals.

You are welcome to present more than one WIP. For example you could present an idea before collecting pilot data, or accessing shared datasets for analysis projects (e.g. UK Biobank), and then present again after data collection, or analysis has started, but not progressed very far. Any time you give a WIP presentation you should be at a stage where you are willing and able to incorporate feedback from the attendees.

## Where does the WIP take place?

- WIP meetings take place as part of the WIN Wednesday meetings on **Wednesdays between 12:00 and 13:00**
- Meetings are hybrid, with the option to join in person or remotely via MS Teams.
- In person participants can join at the [Cowey Seminar Room in the FMRIB Annex](https://www.win.ox.ac.uk/about/how-to-find-us/finding-win-fmrib/meeting-rooms) with a **maximum of 30 participants**, or the [Meeting Room in OHBA](https://www.win.ox.ac.uk/about/how-to-find-us/finding-win-ohba) with a **maximum of 10 participants** (overflow in the OHBA Reception).
- Remote participants can access via the Teams link which will be circulated in the Monday Message.


## How do I sign up for a WIP presentation?

WIP presentations are booked using Calpendo. See [this guide](../calpendo-booking-guide-WIP) for how to book your presentation.

At the point of booking your WIP you will be asked to provide the following information:

- Title of the project
- Name of presenter, Principal Investigator and collaborators. Include the Supervisor if the presenter is a student.
- A signed [speaker recording release](https://help.it.ox.ac.uk/sites/default/files/help/documents/media/downloadable_legal_form_pack_3-10-22.zip). The meeting will be recorded during the presentation only and not during the question and answer session. A signed release should be returned for everyone who intends to contribute to the main presentation.

WIPs can be booked up to 10 days before they are scheduled to take place. This ensures there is time to circulate notice of the WIP in the preceding Monday message, and invite guests.

## How should I promote my WIP?

Your WIP will be announced in the WIN Monday Message. *We recommend that you also directly contact and invite specific people who you would like feedback from, to maximise your opportunity for constructive review*.

You should consider explicitly inviting people you know who have experience with:

- Closely related experimental designs
- Similar patient populations
- Relevant physics expertise
- Relevant analysis expertise
- Relevant programming or software development expertise

If you're unsure of who to invite, please speak to one of the WIP organisers and they can help advise.
