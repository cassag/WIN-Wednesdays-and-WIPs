---
layout: default
title: WIP guide
has_children: true
nav_order: 2
---

# WIP guide
{: .fs-9 }

A guide to preparing for your WIP
{: .fs-6 .fw-300 }

---

**Contents**
- [What is a WIP?](#what-is-a-wip)
- [What to expect from a WIP Presentation](#what-to-expect-from-a-wip-presentation)
- [Why should I do a WIP presentation?](#why-should-i-do-a-wip-presentation)


## What is a WIP?

The Work in Progress (WIP) meetings provide an informal and constructive forum for researchers to get feedback on planned imaging projects from a diverse WIN audience. WIPs are scheduled as part of the WIN Wednesday meetings that take place on Wednesdays 12:00 - 13:00, everyone who is affiliated with the WIN is welcome to attend these meetings.

The WIN Wednesday weekly meetings are a good opportunity to meet WIN researchers from other groups, find out more about the breadth of research being conducted at WIN, contribute to and learn from the discussion.

We invite you to attend WIPs as often as you can, and be part of these integral "WIN Culture" events. WIP presentations are advertised in the WIN Monday Message (circulated through win-messages@maillist.ox.ac.uk); if you are not currently on this list please email computing-help@win.ox.ac.uk asking to be added.

WIPs are a useful opportunity to get expert feedback on any and all research being conducted at WIN. This includes (but is not limited to):

- new imaging data acquisition
- secondary analysis
- sequence development
- new behavioural interventions
- new software applications or extensions
- new analytic techniques
- training materials
- experimental protocols
- public engagement activities

Note *all new MRI and MEG projects running at WIN are required to give a WIP presentation in order to receive approval and a project code*.

Everyone interested in giving a WIP presentation should try to schedule their presentation as early in their project as possible. *WIPs are meant for early stage work, not for projects nearing completion*.

The below information is provided to guide your preparation for and participation in WIP meetings. Please do not hesitate to contact any of the individuals listed in the [help](../WIP-guide-support) section if you would like to discuss any stage of the process or development of your project.

## What to expect from a WIP Presentation

WIPs are an informal meeting and should be viewed as a positive opportunity for feedback from your peers. WIP attendees are reminded that discussions should be constructive, welcoming, accessible, and at all times in accordance with the [University policies on bullying and harassment](https://edu.admin.ox.ac.uk/university-policy-on-harassment). We are all reminded that you are the expert on your topic, and we are here to support you based on our own experience. As WIP organisers, we welcome your feedback on your experience of planning and presenting your WIP, so we can improve the experience for others.

## Why should I do a WIP presentation?

-   To help optimise the quality of your research with feedback from broad perspectives
-   To encourage you to think early about the reproducibility of your research and opportunities to share your materials.
-   To encourage collaboration among WIN members, by creating opportunities to hear about the work others are doing in all disciplines.

The WIP is not a box ticking exercise, or a test. It is a way to share planned work or work in early stages with your WIN peers. A WIP presentation can provide you with valuable feedback from the amazing WIN community, who are all here to support you in doing the best research you can.

## How long is a WIP presentation?

WIP presentations are 20 minutes long (12 minutes presentation + 8 minutes questions).
