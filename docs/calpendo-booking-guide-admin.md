---
layout: default
title: Administators
parent: Calpendo booking guide
has_children: false
nav_order: 3
---

# Guide for Administrators
{: .fs-9 }

Processes relating to WIN Wednesdays for Administrators
{: .fs-6 .fw-300 }

---

**Contents**
- [What do administrators do?](#what-do-administrators-do)
- [Allocation of bookings to WIN Wednesday themes](#allocation-of-bookings-to-win-wednesday-themes)
- [Approval of bookings](#approval-of-bookings)
- [Communication of the processes](#communication-of-the-processes)
- [Hosting and recording via MS Teams](#hosting-and-recording-via-ms-teams)
- [Calpendo testing account](#calpendo-testing-account)
- [Email templates](#email-templates)


## What do administrators do?

WIN Wednesday administrators are responsible for approving WIP and seminar bookings, and liaising with presenters. They also manage the Calpendo resource and can adjust availability in the template.

Administrators can create bookings any time between 06:00-22:00 Monday-Sunday. All admin bookings will be automatically be approved.

Contact a Calpendo Manager (for example sebastian.rieger@psych.ox.ac.uk) to request that members are added or removed from the WIN Wednesday Administrators Calpendo group.

## Allocation of bookings to WIN Wednesday themes

Before the start of the year, allocate slots to each of the speaker themes (e.g. EDI, Educational) as required. Communicate dates with WIN Wednesday Coordinators and ask them to book their slots. Encourage WIN Wednesday Coordinators to enter their bookings on Calpendo before the calendar is made available for WIPs.

Note: It may be wise to communicate annually when "the WIP calendar is now open for bookings", to ensure that all seminars are pre-filled before it goes to wider release.

## Block out "no meeting" slots

An administrator should prevent bookings in the calendar at times when meetings are not possible, for example during the Christmas break. Do this by creating a booking with the description and title/author/abstract "no meeting".

## Generate and assign booker for off-schedule meetings

It is possible to adjust the template to enable bookings outside of the Wednesday 12:00-13:00 slot. If such meetings are sporadic, however, it is easier for an administrator to book the slot at the desired time and transfer ownership meeting to the appropriate coordinator.

1. Select the required coordinator from the list of possible owners. This list shows everyone with a Calpendo account.
2. The owner will not be notified of the booking you have made for them. Set an appropriate email notification period to remind them of the booking.
3. Tick the "send reminders to booking owner" box to ensure they are received by the owner you have selected.

![calpendo administrator booking outside of the template, with arrows highlighting the drop down list to open the list of Calpendo users, required booking owner, and checkbox to ensure email reminder is sent to the owner](../../img/calpendo-owner.png)

The owner will not receive an automated email when you create this booking. It may be useful at this stage to use Calpendo to inform them that you have made a booking on their behalf, and email to request that they add any available details. Click "Send email" then check the "owner" box to include the nominated owner as a recipient.

![calpendo administrator booking outside of the template, with arrows highlighting the send email button and "owner" selection. ](../../img/calpendo-owner-email.png)

On the email composer screen, enter a subject heading and message body. The email will be sent from your email address, but the message will include a line *"Sent by the WIN Booking System. Please do not reply to this email as this address is not monitored."*

![calpendo administrator email composer, with arrows highlighting the message subject, body and send buttons. ](../../img/calpendo-owner-email.png)

## Approval of bookings

### Approval responsibilities

The [WIP coordinator](../calpendo-booking-guide-support) (Cassandra Gould van Praag) will review bookings as they are received. If they are complete they will be approved immediately using the process described below. If they are incomplete the booked will be emailed and reminded of the necessary information. The [WIN Wednesday Seminar coordinator](../calpendo-booking-guide-support) need not action any Calpendo emails.

On the Friday before the Wednesday, the [WIN Wednesday Seminar coordinator](../calpendo-booking-guide-support) will review Calpendo for that week. They will retrieve the required information for the Monday Message directly from Calpendo. Where this information is not complete (i.e. the booking has not already been approved), they will email the booker and inform them that their slot will be cancelled if the information is not promptly received.

### Approval process

#### 1. Find booking

You will be notified by email when a booking has been made. To review and approve that booking, first find it in Calpendo and click on "view".

![calpendo page for Administrators, with an arrow highlighting the "view" link to review a users booking](../../img/calpendo-approve-view.png)

#### 2. Review booking

Review the details of the booking to ensure they are ready to be published in the Monday message. Open and review the recording consent. If any details are missing, use the send email" button to contact the user.

![calpendo page for Administrators, showing how to review another users booking. Arrows highlighting the "Title, Authors and Abstract" field, "Recording consent" and "send email" button](../../img/calpendo-approve-review.png)

#### 3. Approve or deny booking

If all details are present and correct, move on to approve the booking. Click the "Admin" tab at the top of the Calpendo window and select "Booking Requests" to bring up a list of all bookings which require approval.

![calpendo page for Administrators, with arrows highlighting the "Admin" and "Booking Requests" link](../../img/calpendo-approve-bookinglist.png)

Locate the booking you reviewed. It may be helpful to sort the list of unapproved bookings by Resource. Click the checkbox next to the booking you wish to approve and click "Approve" or deny as appropriate.

![calpendo page for Administrators, with an arrow highlighting the "approve" button on a booking made against the WIN Wednesday resource](../../img/calpendo-approve-approve.png)

## Communication of the processes

The [WIN Wednesday Seminar coordinator](../calpendo-booking-guide-support) will include a link in every Monday Message to the WIP and Calpendo booking guides: "See this information to book your own WIP."

## Hosting and recording via MS Teams

WIN Wednesdays can be joined remotely via MS Teams.

The [WIP coordinator](../calpendo-booking-guide-support) will be the host of the call for all seminars where there is a WIP slot. For other WIN Wednesday Seminars where there is no WIP slot, the WIN Wednesday Coordinator will host the call. The host will be responsible for recording the call.

To record the call on MS Team, you will need to have this feature enabled on your Nexus account. **Please see [this guide from IT](https://help.it.ox.ac.uk/record-a-meeting-in-teams) to request that recording is enabled on your account (minimally 48h before the call is scheduled) and for instructions on how to record the call.**

## Calpendo testing account

It may be necessary for administrators to have access to a Calpendo account without administrative privileges, so they can test newly implemented features or rules. A local, non-administrator account can be requested by contacting sebastian.rieger@psych.ox.ac.uk.

## Email templates

The below templates can be used by the WIP coordinator to liaise with researchers after their booking request has been received via calpendo. Text in &lt &gt symbols is to be deleted as appropriate.

### Booking approved

Many thanks for booking your WIP and uploading all the information to caplendo. The booking has now been approved.

You indicated in the booking that you will be presenting <in person / online> <from FMRIB / OHBA>. Please let me know if this changes for any reason.

<If you are presenting remotely, please could you join the call at 11:50 on <DATE>, so we can check AV. >

<On <DATE>, please meet me in the Cowey Room in the FMRIB Annex at <11:45>. You can join the meeting via Teams from your own laptop and present your material as a normal online meeting. Look out for a calendar invite from Andrew for details to join the meeting.>

If you don’t regularly visit FMRIB, please make sure you have access to the FMRIB Annex Building on the day - contact admin@win.ox.ac.uk to confirm.

Please contact open@win.ox.ac.uk if you would like to discuss your plans for sharing data or other outputs of your project (open science)- always easier to start thinking about these at the start! The Open WIN Ambassadors would be very happy to guide you through the complexities or discuss any concerns you may have.

### Add recording consent

Many thanks for booking your WIP on calpendo.

Please could you update the booking to include <the recording consent> when you have decided who will be presenting. We’ll review your booking again before the scheduled presentation and approve the booking if all the required details have been entered.

You indicated in the booking that you will be presenting <in person / online> <from FMRIB / OHBA>. Please let me know if this changes for any reason.

<If you are presenting remotely, please could you join the call at 11:50 on <DATE>, so we can check AV.>

<On <DATE>, please meet me in the Cowey Room in the FMRIB Annex at <11:45>. You can join the meeting via Teams from your own laptop and present your material as a normal online meeting. Look out for a calendar invite from Andrew for details to join the meeting.>

If you don’t regularly visit FMRIB, please make sure you have access to the FMRIB Annex Building on the day - contact admin@win.ox.ac.uk to confirm.

Please contact open@win.ox.ac.uk if you would like to discuss your plans for sharing data or other outputs of your project (open science)- always easier to start thinking about these at the start! The Open WIN Ambassadors would be very happy to guide you through the complexities or discuss any concerns you may have.

### Add all information

Many thanks for booking your WIP with Andrew and gaining your calpendo access..

Please could you update the booking to include the recording consent, title, abstract and authors when you have decided who will be presenting. Information about what to include is provided in this guide. We’ll review your booking again before the scheduled presentation and approve the booking if all the required details have been entered.

In your booking you will be able to indicate if you would like to present in person from FMRIB, in person form OHBA, or remotely. 

If you are presenting remotely, please could you join the call at 11:50 on 22nd Feb, so we can check AV.

If you are presenting in-person from FMRIB please meet me in the Cowey Room in the FMRIB Annex at 11:45. If you don’t regularly visit FMRIB, please make sure you have access to the FMRIB Annex Building on the day - contact admin@win.ox.ac.uk to confirm.

If you would like to present in-person from OHBA, please head to the meeting room for 11:45 and Sarah Clayton will set up the room for you.

In all cases (in person or remote), you will join the meeting via Teams from your own laptop and present your material as a normal online meeting. Look out for a calendar invite from Andrew for details to join the meeting.

Please contact open@win.ox.ac.uk if you would like to discuss your plans for sharing data or other outputs of your project (open science)- always easier to start thinking about these at the start! The Open WIN Ambassadors would be very happy to guide you through the complexities or discuss any concerns you may have.
