---
layout: default
title: What to include
parent: WIP guide
has_children: false
nav_order: 1
---

# What to include in your WIP
{: .fs-9 }

A guide to preparing for your WIP
{: .fs-6 .fw-300 }

---

**Contents**
- [Who should present and be present at a WIP?](#who-should-present-and-be-present-at-a-wip)
- [What should I include in my presentation?](#what-should-i-include-in-my-presentation)
- [What to do after the WIP](#what-to-do-after-the-wip)

## Who should present and be present at a WIP?

Any WIN affiliated researcher undertaking a research project. This includes projects relating to:

- Analysis Research
- Physics Research
- Basic Neuroscience Research
- Clinical Research
- Cognitive neuroscience Research

The individual undertaking the majority of the data collection, analysis or content generation (as appropriate for the project being presented) should lead the presentation. In most cases this will be a student or Early Career Researcher.

PIs and Supervisors must be present to support and provide additional information where required. Named collaborators (internal and external) should ideally also be present.

You should also identify any tools (such as specific FSL packages), approaches, behavioural tasks, pulse sequences or datasets you will be using. If you think you will need additional support using these tools, outside that which is available in your research group, please contact the developers and ensure they are invited to your WIP. Please review the [WIN authorship guidelines](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/SitePages/WIN%20Authorship%20Guide%202021.aspx) for more details on this level of collaboration and the list of [WIN tools, along with tool creators](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/SitePages/WIN%20Standard%20Acknowledgement%20and%20References.aspx)

## What should I include in my presentation?

You should present an outline of your intended experiment or analysis, including a brief outline of the motivation, background or justification for the research.

A rough suggested outline of a presentation for new data acquisition is below:

- List main aim/question of the study. Provide brief background, hypotheses or describe exploratory motivations.
- List and describe tasks if applicable (Preclinical/Clinical/Cogneuro). Comment on plans to share task materials internally or externally.
- List and describe analysis methods.
- List expected results. Describe why they are expected.
- List protocol details (MRI/MEG/EEG) and comment on any existing or planned uploads to the Open Acquisition database.
- List tools/software requirements (if needed) and comment on the reproducibility or your analysis pipeline, for example plans to publish your code from the WIN GitLab instance
- List any datasets you might need access to
- Describe your plans for [sharing your research outputs](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/) (imaging or otherwise) and any restrictions which may prevent sharing at this time.
- Describe your plans for [Patient and Public Involvement & Engagement (PPI&E)](../WIP-guide-support#patient-and-public-involvement--engagement)


## What to do after the WIP

- Try to incorporate any feedback you were given
- Follow up with main discussion contributors
- Always feel free to ask for more input if needed
- Get a project code if needed (or start the process)
